import org.cleanmobilecode.Project
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class CoCodersTest {
    @Test
    fun `Test that we can create a Project with a default empty description`() {
        assertEquals(Project("tom"), Project("tom"))
    }

    @Test
    fun `Test that we can create a project description`() {
        val description = "my really cool project"
        assertEquals(description, Project("tom", description).description)
    }

    @Test
    fun `Test that we have an empty language collection`() {
        val project = Project("tom")
        assertEquals(0, project.languages.size)
    }
}
